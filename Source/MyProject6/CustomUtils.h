// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include <functional>
#include "CustomUtils.generated.h"

/**
 * 
 */
UCLASS()
class MYPROJECT6_API UCustomUtils : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
public:
		UFUNCTION(BlueprintPure,BlueprintCallable)
		static TMap<float, AActor*>& SortMap(UPARAM(ref) TMap<float,AActor*>& map) {
			map.KeySort(std::greater<float>());
			return map;
				
		}
		
		
		UFUNCTION(BlueprintPure,BlueprintCallable)
		static FRotator RotFromQuat(const FQuat& quat) {
			
			return quat.Rotator();
				
		}
	
	
};
