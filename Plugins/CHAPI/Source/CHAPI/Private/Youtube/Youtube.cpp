#include "Youtube.h"
#include "Http.h"	

void UYoutube::LoginComplete(const FString & url)
{
	FRegexPattern TokenPattern(TEXT("(access_token=)(\\w+)"));
	FRegexMatcher TokenMatcher(TokenPattern, url);
	if (TokenMatcher.FindNext()) {
		Oauth = TokenMatcher.GetCaptureGroup(2);
	}
	else {
		return;
	}
	auto req = FHttpModule::Get().CreateRequest();
	requests.Add(req);
	FString RequestUrl = FString::Format(TEXT("https://www.googleapis.com/oauth2/v1/tokeninfo?access_token={0}"), {Oauth});

	
	

}

void UYoutube::ConnectToChannel(const FString& Channel)
{

}

void UYoutube::SendMsg(const FString& Channel, const FString& Msg)
{

}

void UYoutube::Init() {
	FString AuthUrl = FString::Format(TEXT("https://accounts.google.com/o/oauth2/auth?client_id={0}&redirect_uri={1}&scope=https://www.googleapis.com/auth/youtube&response_type=token"),
		{ ClientID,RedirectURL });
	OnLoginRequest.Broadcast(AuthUrl);
}