#include "Twitch.h"
#include "IRCClient.h"
#include "Regex.h"
#include "Http.h"



DEFINE_LOG_CATEGORY(LogCHAPI);

void UTwitch::Init()
{
	FString AuthUrl = FString::Format(TEXT("https://id.twitch.tv/oauth2/authorize?client_id={0}&redirect_uri={1}&response_type=token&scope=chat_login"),
		{ ClientID,RedirectURL });
	OnLoginRequest.Broadcast(AuthUrl);
	
}

void UTwitch::ConnectToChannel(const FString& Channel)
{
	IRCSendRaw(FString::Format(TEXT("JOIN #{0}"), { Channel}));
}

void UTwitch::SendMsg(const FString& Channel, const FString& Msg)
{

	IRCSendRaw(FString::Format(TEXT("PRIVMSG #{0} :{1}"), { Channel ,Msg }));
}

void UTwitch::IRCSendRaw(const FString& msg)
{
	if (_IRCClient.IsValid()) {
		_IRCClient->Send(msg);
	}
}


TMap<FString, FString> UTwitch::GenerateHeaders(const TArray<std::string>& params) {
	TMap<FString, FString> h;
	for (const auto& p : params) {
		if (p == "Accept") {
			h.Add(TEXT("Accept")) = TEXT("application/vnd.twitchtv.v5+json");
		}
		else if (p == "Client-ID") {
			h.Add(TEXT("Client-ID")) = ClientID;
		}
		else if (p == "Authorization") {
			FString OauthWithoutOauth = Oauth;
			OauthWithoutOauth.RemoveFromStart(TEXT("oauth:"));

			FString AuthorizationValue("OAuth ");
			AuthorizationValue.Append(OauthWithoutOauth);


			h.Add(TEXT("Authorization")) = AuthorizationValue;
		}
	}
	return h;
}



void UTwitch::LoginComplete(const FString& url)
{
	FRegexPattern TokenPattern(TEXT("(access_token=)(\\w+)"));
	FRegexMatcher TokenMatcher(TokenPattern, url);
	if (TokenMatcher.FindNext()) {
		Oauth =TokenMatcher.GetCaptureGroup(2);
	}
	else {
		return;
	}
	auto req = FHttpModule::Get().CreateRequest();
	requests.Add(req);
	FString RequestUrl;

	RequestUrl = TEXT("https://api.twitch.tv/kraken");
	
	FString AuthorizationValue("OAuth ");
	AuthorizationValue.Append(Oauth);

	req->SetURL(RequestUrl);
	req->SetHeader(TEXT("Accept"), TEXT("application/vnd.twitchtv.v5+json"));
	req->SetHeader(TEXT("Authorization"), AuthorizationValue);
	req->OnProcessRequestComplete().BindLambda([this](FHttpRequestPtr a,
		FHttpResponsePtr b,
		bool c) {
		if (!this->IsValidLowLevel() || !b.IsValid() || !a.IsValid()) {

			return;
		}
		auto str = b->GetContentAsString();
		OnResponseHTTP.Broadcast(str);
		requests.Remove(a);
		FRegexPattern UserNamePattern(TEXT("(\"(user_name)\"):(\"(\\w+)\")"));
		FRegexPattern ClientIDPattern(TEXT("(\"(client_id)\"):(\"(\\w+)\")"));
		FRegexMatcher UserNameMatcher(UserNamePattern, str);
		FRegexMatcher ClientIDMatcher(ClientIDPattern, str);
		FString access_token;
		if (UserNameMatcher.FindNext() && ClientIDMatcher.FindNext()) {
			_Nick = UserNameMatcher.GetCaptureGroup(4);
			FIPv4Address adr(52, 27, 133, 22);
			FIPv4Endpoint ep(adr, 6667);
			FString OauthWithOauth(TEXT("oauth:"));
			OauthWithOauth.Append(Oauth);
			_IRCClient = MakeShared<IRCClient>(
				ep,
				_Nick,
				OauthWithOauth);
			_IRCClient->Init();
			_IRCClient->AddHandler([this](const FString& msg) {
				if (!this->IsValidLowLevel()) {

					return;
				}
				OnResponseIRC.Broadcast(msg);
			});
			
			_IRCClient->AddHandler([this](const FString& msg) {
				if (!this->IsValidLowLevel()) {

					return;
				}
				FRegexPattern MsgPattern(TEXT(":([a-zA-Z0-9_]{4,25})!\\1@\\1\\.tmi\\.twitch\\.tv PRIVMSG #([a-zA-Z0-9_]{4,25}) :(.+)"));
				FRegexMatcher MsgMatcher(MsgPattern, msg);

				if (MsgMatcher.FindNext()) {
					FString User = MsgMatcher.GetCaptureGroup(1);
					FString Channel = MsgMatcher.GetCaptureGroup(2);
					FString Message = MsgMatcher.GetCaptureGroup(3);
					OnChatMessage.Broadcast(Channel, User, Message);
				}
			});
		}

	});

	req->ProcessRequest();
}

