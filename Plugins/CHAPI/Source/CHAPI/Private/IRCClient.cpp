#include "IRCClient.h"
#include "memory"
#include "ScopeLock.h"
#include "Regex.h"

//DEFINE_LOG_CATEGORY(LogIRCClient);


IRCClient::IRCClient(FIPv4Endpoint addr, const FString& nick, const FString& pass):
	_addr(addr),
	_nick(nick),
	_pass(pass)
{
	
	
}

bool IRCClient::Init()
{
	_sock = MakeShared<CHAPISocket>(_addr);
	
	FString pass(TEXT("PASS "));
	
	pass.Append(_pass);
	
	Send(pass);

	FString nick(TEXT("NICK "));
	
	nick.Append(_nick);

	Send(nick);
	
	AddHandler([this](const FString& str) {
		if (str.Contains(TEXT("PING"))) {
			Send(TEXT("PONG"));
	}
	});
	return true;
}

bool IRCClient::Send(const FString& msg)
{
	TArray<uint8> InfoToSend;
	InfoToSend.Reserve(msg.Len());
	std::string utf8msg = TCHAR_TO_UTF8(*msg);

	for (const auto& ch : utf8msg) {
		InfoToSend.Add(ch);
	}
	InfoToSend.Add('\n');
	return _sock->Send(InfoToSend);
}

void IRCClient::AddHandler(std::function<void(FString)> h)
{

	_sock->AddHandler([h](const TArray<uint8>& data) {
		FString resieved;
		std::string b;
		b.reserve(data.Num());
		resieved.Reserve(data.Num());
		for (const auto& d : data) {
			b.push_back((char)d);
		}
		resieved = UTF8_TO_TCHAR(b.c_str());
		h(resieved);

	});

}

IRCClient::~IRCClient()
{
	UE_LOG(LogTemp, Log, TEXT("IRCClient destructed"));
}

