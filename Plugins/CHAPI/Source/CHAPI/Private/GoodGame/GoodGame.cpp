#include "GoodGame.h"





void UGoodGame::Init(const FString& Login, const FString& Pass)
{
	auto req = FHttpModule::Get().CreateRequest();
	requests.Add(req);
	FString url(TEXT("https://goodgame.ru/ajax/chatlogin"));
	
	
	
	FModuleManager::Get().LoadModule(TEXT("WebSockets"));
	

	req->SetURL(url);
	req->SetHeader(TEXT("Content-Type"), TEXT("application/x-www-form-urlencoded"));
	
	req->SetContentAsString(FString::Format(TEXT("login={0}&password={1}"), { Login,Pass }));
	req->OnProcessRequestComplete().BindLambda([this](FHttpRequestPtr a,
		FHttpResponsePtr b,
		bool c) {
		if (!this->IsValidLowLevel() || !b.IsValid() || !a.IsValid()) {

			return;
		}
		requests.Remove(a);
		try {
			nlohmann::json j = nlohmann::json::parse(TCHAR_TO_UTF8(*b->GetContentAsString()));
			std::string resp(j["response"].get<std::string>().c_str());

			FString response(UTF8_TO_TCHAR(resp.c_str()), resp.size());
			UE_LOG(LogCHAPI, Log, TEXT("%s"), *response);
			_User_ID = FString(j["user_id"].get<std::string>().c_str());
			_Token = FString(j["token"].get<std::string>().c_str());
			
			nlohmann::json auth;
			
			auth["type"] = "auth";
			auth["data"]["user_id"] = TCHAR_TO_ANSI(*_User_ID);
			auth["data"]["token"] = TCHAR_TO_ANSI(*_Token);
			
			_sock = FWebSocketsModule::Get().CreateWebSocket(TEXT("ws://chat.goodgame.ru:8081/chat/websocket"));
			
			
			auto dumped = auth.dump();
			FString s = dumped.c_str();
			
			
			_sock->OnConnected().AddLambda(
				[this]() {
				UE_LOG(LogCHAPI, Log, TEXT("Goodgame websock connected"));
			}
			);
			_sock->OnMessage().AddLambda(
				[this](const FString & msg) {
				UE_LOG(LogCHAPI, Log, TEXT("%s"),*msg);
				
					if (!this->IsValidLowLevel()) {
						return;
					}
					try {
						nlohmann::json success_join_response_json = nlohmann::json::parse(TCHAR_TO_UTF8(*msg));
						if (bJustConnectToChannel) {
						
						
							bJustConnectToChannel = false;
							bool key = false;
							
							if(FString(UTF8_TO_TCHAR(success_join_response_json["type"].get<std::string>().c_str())) == FString(TEXT("success_join"))){
								ConnectedChannels.Add(success_join_response_json["data"]);
								key = true;
							}
							
							if (key) {
								nlohmann::json websock_request_json;
								websock_request_json["type"] = "send_message";
								websock_request_json["data"]["channel_id"] = InProgressChannel;
								websock_request_json["data"]["text"] = TCHAR_TO_UTF8(*JustConnectToChannelMsg);
								websock_request_json["data"]["hideIcon"] = false;
								websock_request_json["data"]["mobile"] = false;
								auto dumped = websock_request_json.dump(-1, ' ', true);

								_sock->Send(dumped.c_str());
							}
						}
						if (FString(UTF8_TO_TCHAR(success_join_response_json["type"].get<std::string>().c_str()))== FString(TEXT("message"))) {
							FString url = FString::Format(TEXT("http://goodgame.ru/api/getchannelstatus?id={0}&fmt=json"), { success_join_response_json["data"]["channel_id"].get<int32_t>() });
							auto req = FHttpModule::Get().CreateRequest();
							requests.Add(req);
							req->SetURL(url);
							req->OnProcessRequestComplete().BindLambda([this, success_join_response_json](FHttpRequestPtr a,
								FHttpResponsePtr b,
								bool c) {
								if (!this->IsValidLowLevel() || !b.IsValid() || !a.IsValid()) {
									
									return;
								}
								try {
									nlohmann::json responsejson = nlohmann::json::parse(TCHAR_TO_UTF8(*b->GetContentAsString()));

									OnChatMessage.Broadcast(
										FString(UTF8_TO_TCHAR((*responsejson.begin())["title"].get<std::string>().c_str())),
										UTF8_TO_TCHAR(success_join_response_json["data"]["user_name"].get<std::string>().c_str()),
										UTF8_TO_TCHAR(success_join_response_json["data"]["text"].get<std::string>().c_str()));
								}
								catch (std::exception& e) {

										UE_LOG(LogCHAPI, Error, TEXT("%s "), UTF8_TO_TCHAR(e.what()));


									}
							});
							req->ProcessRequest();
						}
					}
					catch (std::exception& e) {

						UE_LOG(LogCHAPI, Error, TEXT("%s "), UTF8_TO_TCHAR(e.what()));


					}
				}
			);
			_sock->Connect();
			_sock->Send(s);
			
		}
		catch (std::exception& e) {
			
			UE_LOG(LogCHAPI, Error, TEXT("%s ") , UTF8_TO_TCHAR(e.what()));
			
			
		}
		
	});
	req->SetVerb(TEXT("POST"));
	req->ProcessRequest();

}

void UGoodGame::ConnectToChannel(const FString& Channel)
{
	if (!_sock.IsValid()) {
		return;
	}
	FString url = FString::Format(TEXT("http://goodgame.ru/api/getchannelstatus?id={0}&fmt=json"), { Channel });
	auto req = FHttpModule::Get().CreateRequest();
	requests.Add(req);
	req->SetURL(url);

	req->OnProcessRequestComplete().BindLambda([this](FHttpRequestPtr a,
		FHttpResponsePtr b,
		bool c) {
		if (!this->IsValidLowLevel() || !b.IsValid() || !a.IsValid()) {

			return;
		}
		requests.Remove(a);
		try {
			nlohmann::json responsejson = nlohmann::json::parse(TCHAR_TO_UTF8(*b->GetContentAsString()));
			//std::string resp(responsejson["response"].get<std::string>().c_str());
			nlohmann::json websock_join_request_json;
			websock_join_request_json["type"] = "join";
			websock_join_request_json["data"]["channel_id"] = responsejson.begin().key();
			websock_join_request_json["data"]["hidden"] = false;
			auto dumped_websock_join_request_json = websock_join_request_json.dump();

			_sock->Send(dumped_websock_join_request_json.c_str());


		}
		catch (std::exception& e) {

			UE_LOG(LogCHAPI, Error, TEXT("%s "), UTF8_TO_TCHAR(e.what()));


		}

	});
	//req->SetVerb(TEXT("POST"));
	req->ProcessRequest();
}


void UGoodGame::SendMsg(const FString& Channel, const FString& Msg)
{
	if (!_sock.IsValid()) {
		return;
	}
	FString url = FString::Format(TEXT("http://goodgame.ru/api/getchannelstatus?id={0}&fmt=json"), { Channel });
	auto req = FHttpModule::Get().CreateRequest();
	requests.Add(req);
	req->SetURL(url);
	
	req->OnProcessRequestComplete().BindLambda([this,Msg, Channel](FHttpRequestPtr a,
		FHttpResponsePtr b,
		bool c) {
		if (!this->IsValidLowLevel() || !b.IsValid() || !a.IsValid()) {

			return;
		}
		requests.Remove(a);
		try {
			nlohmann::json responsejson = nlohmann::json::parse(TCHAR_TO_UTF8(*b->GetContentAsString()));
			
			
			
			if (!ConnectedChannels.FindByPredicate([this, responsejson](const auto& e) {
				//UE_LOG(LogCHAPI, Log, TEXT("%s"), *FString(UTF8_TO_TCHAR(e["channel_name"].get<std::string>().c_str())));
				//return FString(UTF8_TO_TCHAR(e["channel_id"].get<std::string>().c_str())) == responsejson.begin().key();
				return e["channel_id"].get<int32_t>() == std::stoi(responsejson.begin().key());
			})) {
				
				nlohmann::json websock_join_request_json;
				websock_join_request_json["type"] = "join";
				websock_join_request_json["data"]["channel_id"] = responsejson.begin().key();
				websock_join_request_json["data"]["hidden"] = false;
				auto dumped_websock_join_request_json = websock_join_request_json.dump();
				bJustConnectToChannel = true;
				JustConnectToChannelMsg = Msg;
				InProgressChannel = responsejson.begin().key();
				_sock->Send(dumped_websock_join_request_json.c_str());
			}
			else {
				try {
					nlohmann::json websock_request_json;
					websock_request_json["type"] = "send_message";
					websock_request_json["data"]["channel_id"] = responsejson.begin().key();
					websock_request_json["data"]["text"] = TCHAR_TO_UTF8(*Msg);
					websock_request_json["data"]["hideIcon"] = false;
					websock_request_json["data"]["mobile"] = false;
					auto dumped = websock_request_json.dump(-1, ' ', true);

					_sock->Send(dumped.c_str());
						
					
				}
				catch (std::exception& e) {

					UE_LOG(LogCHAPI, Error, TEXT("%s "), UTF8_TO_TCHAR(e.what()));


				}
			}
			
			
			
			

		}
		catch (std::exception& e) {

			UE_LOG(LogCHAPI, Error, TEXT("%s "), UTF8_TO_TCHAR(e.what()));


		}

	});
	
	//req->SetVerb(TEXT("POST"));
	
	
	req->ProcessRequest();


	
}

UGoodGame::~UGoodGame() {
	FModuleManager::Get().UnloadModule(TEXT("WebSockets"));
}
