#include "CHAPISocket.h"
#include "ScopeLock.h"
#include <iterator>

#include "RunnableThread.h"



CHAPISocket::CHAPISocket(FIPv4Endpoint addr):
	_addr(addr),
	bStoping(false)
{
	_thr = TSharedPtr<FRunnableThread>(FRunnableThread::Create(this, TEXT("CHAPISocketThread")));
}

uint32 CHAPISocket::Run()
{
	for (;;) {
		if (bStoping ||_sock->GetConnectionState() != ESocketConnectionState::SCS_Connected) {
			return 0;
		}
		TArray<TArray<uint8>> tmpMsgQueue;
		if (_MsgQueue.Num() > 0) {
			mutex.Lock();
			tmpMsgQueue = _MsgQueue;
			mutex.Unlock();
		}
		for (auto& msg : tmpMsgQueue) {

			int32 sended = 0;
			if (_sock.IsValid() 
				&& _sock->GetConnectionState() == ESocketConnectionState::SCS_Connected
				&& _sock->Send(msg.GetData(), msg.Num(), sended)
				) {
				
			}
			else {
				return 0;
			}
			
		}
		
		for (const auto& elem : tmpMsgQueue) {
			_MsgQueue.Remove(elem);
		}
		
		uint32 recved = 0;
		if (_sock.IsValid() && _sock->HasPendingData(recved)) {

			TSharedPtr<uint8> RecvBuf(new uint8[recved], [](uint8* data) {delete[] data; });
			int32 readed = 0;
			_sock->Recv(RecvBuf.Get(), recved, readed);
			TArray<uint8> msg;
			msg.Reserve(readed);
			for (int index = 0; index < readed; ++index) {
				msg.Add(RecvBuf.Get()[index]);
			}
			
			for (const auto& h: Handlers) {
				h(msg);
			}
			
		}
	}
}

bool CHAPISocket::Init()
{
	FTcpSocketBuilder b(TEXT("CHAPISock"));
	_sock = TSharedPtr<FSocket>(b.AsBlocking().AsReusable().Build());
	if (!_sock->Connect(*_addr.ToInternetAddr())) {
		//UE_LOG(LogCHAPI, Error, TEXT("Connect Error"));
		return false;

	}
	return true;
}

void CHAPISocket::AddHandler(std::function<void(const TArray<uint8>&)> h)
{
	FScopeLock lock(&mutex);
	Handlers.Add(h);
}

bool CHAPISocket::Send(const TArray<uint8>& data)
{

	FScopeLock lock(&mutex);
	if (_sock->GetConnectionState() != ESocketConnectionState::SCS_Connected) {
		return false;
	}
	_MsgQueue.Push(data);
	return true;
}



CHAPISocket::~CHAPISocket()
{
	bStoping = true;
	if (_thr.IsValid()) {
		_thr->WaitForCompletion();
	}
}

ESocketConnectionState CHAPISocket::GetState()
{
	return _sock->GetConnectionState();
}

