#pragma once
#include "StreamingService.h"
#include "IRCClient.h"
#include "IHttpRequest.h"
#include "Twitch.generated.h"


DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnResponseFromIRC, FString, response);

UCLASS(BlueprintType, config=CHAPI)
class UTwitch:public UStreamingService {

	GENERATED_BODY()
public:


	UPROPERTY(BlueprintAssignable)
		FOnResponseFromHTTP OnResponseHTTP;

	UPROPERTY(BlueprintAssignable)
		FOnResponseFromIRC OnResponseIRC;

	UFUNCTION(BlueprintCallable)
	void Init();

	UFUNCTION(BlueprintCallable)
		virtual void ConnectToChannel(const FString& Channel)override;

	UFUNCTION(BlueprintCallable)
		virtual void SendMsg(const FString& Channel,const FString& Msg)override;

	UFUNCTION(BlueprintCallable)
	void IRCSendRaw(const FString& msg);

	UPROPERTY(Config)
		FString ClientID;

	UPROPERTY(Config)
		FString RedirectURL;


	UFUNCTION(BlueprintCallable)
		void LoginComplete(const FString& url);

    

	TMap<FString, FString> GenerateHeaders(const TArray<std::string>& params);


private:
	FString Oauth;
	TSharedPtr<IRCClient> _IRCClient;
	TArray<FHttpRequestPtr> requests;
	FString _Nick;
};
