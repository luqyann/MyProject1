#pragma once
#include "CoreMinimal.h"
#include "List.h"
#include <functional>
#include <mutex>
#include "CHAPISocket.h"
#include "CriticalSection.h"


//DECLARE_LOG_CATEGORY_EXTERN(LogIRCClient, Log, All);

class IRCClient{
	TSharedPtr<CHAPISocket> _sock;
	FIPv4Endpoint _addr;
	FString _nick, _pass;
public:
	IRCClient(FIPv4Endpoint addr, const FString& nick, const FString& pass);
	bool Init();
	bool Send(const FString& msg);
	void AddHandler(std::function<void(FString)> h);
	~IRCClient();
};

