#pragma once
#include "Engine.h"
#include "StreamingService.generated.h"

DECLARE_LOG_CATEGORY_EXTERN(LogCHAPI, Log, All);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnLoginRequest, FString, url);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnResponseFromHTTP, FString, response);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FOnChatMessage, FString, Channel, FString, User, FString, Msg);

UCLASS(BlueprintType)
class CHAPI_API UStreamingService: public UObject{
	GENERATED_BODY()
public:
	UPROPERTY(BlueprintAssignable)
		FOnLoginRequest OnLoginRequest;

	UPROPERTY(BlueprintAssignable)
	FOnChatMessage OnChatMessage;

	/*for recieve messages*/
	
		virtual void ConnectToChannel(const FString& Channel);

	
		virtual void SendMsg(const FString& Channel, const FString& msg);

};