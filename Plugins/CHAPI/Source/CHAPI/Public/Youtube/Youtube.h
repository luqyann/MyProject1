#pragma once
#include "StreamingService.h"
#include "IHttpRequest.h"
#include "Youtube.generated.h"



UCLASS(BlueprintType,Config=CHAPI)
class UYoutube :public UStreamingService {
	GENERATED_BODY()
public:
	UFUNCTION(BlueprintCallable)
		void Init();

	UPROPERTY(Config)
		FString RedirectURL;

	UPROPERTY(Config)
		FString ClientID;

	
	UFUNCTION(BlueprintCallable)
		void LoginComplete(const FString& url);

	UFUNCTION(BlueprintCallable)
		virtual void ConnectToChannel(const FString& Channel)override;

	UFUNCTION(BlueprintCallable)
		virtual void SendMsg(const FString& Channel, const FString& Msg)override;
private:
	TArray<FHttpRequestPtr> requests;
	FString Oauth;
};
