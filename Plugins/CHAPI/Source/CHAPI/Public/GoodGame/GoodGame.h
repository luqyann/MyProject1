#pragma once
#include "StreamingService.h"
#include "Http.h"
#include "IHttpRequest.h"
#include "CHAPISocket.h"
#include "WebSocketsModule.h"
#include "IWebSocket.h"
THIRD_PARTY_INCLUDES_START
#include "nlohmann/json.hpp"
THIRD_PARTY_INCLUDES_END
#include "GoodGame.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnGoodgameChatMessage, FString, msg);

UCLASS(BlueprintType)
class UGoodGame :public UStreamingService {
	GENERATED_BODY()
public:


	UFUNCTION(BlueprintCallable)
	void Init(const FString& Login, const FString& Pass);

	UFUNCTION(BlueprintCallable)
		virtual void ConnectToChannel(const FString& Channel)override;

	UFUNCTION(BlueprintCallable)
		virtual void SendMsg(const FString& Channel, const FString& Msg)override;

	~UGoodGame();

private:
	bool bJustConnectToChannel = false;
	FString JustConnectToChannelMsg;
	TArray<nlohmann::json> ConnectedChannels;
	std::string InProgressChannel;
	TSharedPtr<TBaseDelegate<void, const FString &>> success_join_delegate;
	TSharedPtr<IWebSocket> _sock;
	FString _User_ID;
	FString _Token;
	TArray<std::function<void(const FString&)>> OnMessageHandlers;
	TArray<FHttpRequestPtr> requests;
};