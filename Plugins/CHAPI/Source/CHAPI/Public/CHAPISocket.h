#pragma once
#include "CoreMinimal.h"
#include "Runnable.h"
#include <functional>
#include <mutex>
#include "AllowWindowsPlatformTypes.h"
#include "TcpSocketBuilder.h"
#include "HideWindowsPlatformTypes.h"


class CHAPISocket :public FRunnable {
public:
	CHAPISocket(FIPv4Endpoint addr);
	uint32 Run()override;
	bool Init()override;
	void AddHandler(std::function<void(const TArray<uint8>&)> h);
	bool Send(const TArray<uint8>& data);

	virtual ~CHAPISocket();

	ESocketConnectionState GetState();
private:
	bool bStoping;
	TArray<TArray<uint8>> _MsgQueue;
	TArray<std::function<void(const TArray<uint8>&)>> Handlers;
	FCriticalSection mutex;
	TSharedPtr<class FRunnableThread> _thr;
	FIPv4Endpoint _addr;
	TSharedPtr<class FSocket> _sock;
};