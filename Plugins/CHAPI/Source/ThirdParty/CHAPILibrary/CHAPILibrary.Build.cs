// Fill out your copyright notice in the Description page of Project Settings.

using System.IO;
using UnrealBuildTool;

public class CHAPILibrary : ModuleRules
{
	public CHAPILibrary(ReadOnlyTargetRules Target) : base(Target)
	{
		Type = ModuleType.External;

		
		
		if (Target.Platform == UnrealTargetPlatform.Win64)
		{
			// Add the import library
			//PublicLibraryPaths.Add(Path.Combine(ModuleDirectory, "x64", "Release"));
			
			//PublicIncludePaths.Add(@"c:\lib\boost_1_66_0\");
			//PublicIncludePaths.Add(@"c:\lib\openssl-1.1.1-pre3\include\");
			//PublicIncludePaths.Add(@"c:\lib\curl-7.59.0\include\");
            PublicIncludePaths.Add(@"c:\lib\json-develop\single_include\");
			//PublicLibraryPaths.Add(@"c:\lib\lib\");
			//PublicAdditionalLibraries.AddRange(
			//new string[] {
			//	"Crypt32.lib                                 ",
			//	//"libboost_chrono-vc141-mt-x64-1_66.lib        "   ,
			//	//"libboost_date_time-vc141-mt-x64-1_66.lib     "   ,
			//    //"libboost_regex-vc141-mt-x64-1_66.lib         " ,
			//    //"libboost_serialization-vc141-mt-x64-1_66.lib " ,
			//    //"libboost_system-vc141-mt-x64-1_66.lib        " ,
			//    //"libboost_thread-vc141-mt-x64-1_66.lib        " ,
			//    //"libboost_wserialization-vc141-mt-x64-1_66.lib" ,
			//    "libcrypto.lib                                " ,
			//    "libssl.lib                                   " ,
			//	"libcurl.lib"
			//} 
			//);
            //PublicDefinitions.Add("CURL_STATICLIB");
			/*
            foreach (string path in PublicLibraryPaths) {
               string[] files = Directory.GetFiles(path);
                foreach(string file in files){
                    if (file.Contains(".lib"))
                    {
                        PublicAdditionalLibraries.Add(Path.GetFileName(file));
                    }
                }
            }
            */



        }
        
	}
}
