#include "CustomHitButton.h"
#include "Engine/Texture2D.h"

FReply SCustomHitButton::OnMouseMove(const FGeometry& MyGeometry, const FPointerEvent& MouseEvent) {

	if (!AdvancedHitTexture ||
		!AdvancedHitTexture->PlatformData ||
		AdvancedHitTexture->PlatformData->PixelFormat != PF_B8G8R8A8||
		AdvancedHitTexture->MipGenSettings.GetValue() != TMGS_NoMipmaps
		) {
		
		//UE_LOG(LogTemp, Warning, TEXT("Hit texture must be B8G8R8A8 and NoMipmaps"));
		return SButton::OnMouseMove(MyGeometry, MouseEvent);
	}


	bool WhatToReturn = true;
	FVector2D LocalPosition = MyGeometry.AbsoluteToLocal(MouseEvent.GetScreenSpacePosition());

	LocalPosition.X = floor(LocalPosition.X);
	LocalPosition.Y = floor(LocalPosition.Y);
	LocalPosition /= MyGeometry.GetLocalSize();
	LocalPosition.X *= AdvancedHitTexture->PlatformData->SizeX;
	LocalPosition.Y *= AdvancedHitTexture->PlatformData->SizeY;

	int BufferPosition = (floor(LocalPosition.Y) * AdvancedHitTexture->PlatformData->SizeX) + LocalPosition.X;
	FColor* ImageData = static_cast<FColor*>((AdvancedHitTexture->PlatformData->Mips[0]).BulkData.Lock(LOCK_READ_ONLY));
	if (!ImageData || ImageData[BufferPosition].A <= AdvancedHitAlpha) {

		WhatToReturn = false;
	}


	AdvancedHitTexture->PlatformData->Mips[0].BulkData.Unlock();

	if (WhatToReturn != bIsHovered) {
		bIsHovered = WhatToReturn;
		if (bIsHovered) {
			SButton::OnMouseEnter(MyGeometry, MouseEvent);
		}
		else {
			SButton::OnMouseLeave(MouseEvent);
		}
	}



	return SButton::OnMouseMove(MyGeometry, MouseEvent);
}

FCursorReply SCustomHitButton::OnCursorQuery(const FGeometry& MyGeometry, const FPointerEvent& CursorEvent) const {
	if (!bIsHovered) return FCursorReply::Unhandled();
	TOptional<EMouseCursor::Type> TheCursor = Cursor.Get();
	return (TheCursor.IsSet()) ? FCursorReply::Cursor(TheCursor.GetValue()) : FCursorReply::Unhandled();
}

void UCustomHitButton::SynchronizeProperties() {
	Super::SynchronizeProperties();
	(static_cast<SCustomHitButton*>(MyButton.Get()))->SetAdvancedHitTexture(AdvancedHitTexture);
	(static_cast<SCustomHitButton*>(MyButton.Get()))->SetAdvancedHitAlpha(AdvancedHitAlpha);
}

TSharedRef<SWidget> UCustomHitButton::RebuildWidget() {
	TSharedPtr<SCustomHitButton> CustomHitButtonButton = SNew(SCustomHitButton)
		.OnClicked(BIND_UOBJECT_DELEGATE(FOnClicked, SlateHandleClicked))
		.OnPressed(BIND_UOBJECT_DELEGATE(FSimpleDelegate, SlateHandlePressed))
		.OnReleased(BIND_UOBJECT_DELEGATE(FSimpleDelegate, SlateHandleReleased))
		.OnHovered_UObject(this, &ThisClass::SlateHandleHovered)
		.OnUnhovered_UObject(this, &ThisClass::SlateHandleUnhovered)
		.ButtonStyle(&WidgetStyle)
		.ClickMethod(ClickMethod)
		.TouchMethod(TouchMethod)
		.IsFocusable(IsFocusable)
		;
	
	CustomHitButtonButton->SetAdvancedHitTexture(AdvancedHitTexture);
	CustomHitButtonButton->SetAdvancedHitAlpha(AdvancedHitAlpha);

	MyButton = CustomHitButtonButton;

	if (GetChildrenCount() > 0) Cast<UButtonSlot>(GetContentSlot())->BuildSlot(MyButton.ToSharedRef());

	return MyButton.ToSharedRef();
}
